﻿using System;
using System.Linq;
using System.Web.Mvc;
using DAL.Model;
using DAL;
using BAL.Services;
using BAL.DTO;

namespace SmallProject.Controllers
{
    public class HomeController : Controller
    {


        private IRepository userRepository = new Repository();
        private IService userService = new UserService();
        Mapper mp = new Mapper();
        [HttpGet]
        public JsonResult GetAllUsers()
        {
            var data = mp.ListUser();
            return new JsonResult() { Data = data, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }
     
        [HttpPost]
        public JsonResult CreateUser(User user)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    userRepository.AddUser(user);
                    userRepository.Save();

                    return Json(new { redirectUrl = Url.Action("Index", "Home") });
                }
                catch (Exception ex)
                {
                    ViewBag.Error = "Error" + ex;
                }
            }
            return Json(new
            {
                errors = ModelState.Keys.SelectMany(i => ModelState[i].Errors).Select(m => m.ErrorMessage).ToArray()
            });
        }

        public string DeleteUser(int UserId)
        {
            if (ModelState.IsValid)
            {
                userRepository.DeleteUser(UserId);
                userRepository.Save();
                return "Данные удалены!";
            }
            else
            {
                ModelState.AddModelError("", "Error");
                return "Что-то пошло не так!";
            }
        }
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            var counUsers = userService.CountUser();
            ViewModel user = new ViewModel()
            {

                CountUsers = counUsers
            };

            ViewBag.C = counUsers;
         

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}