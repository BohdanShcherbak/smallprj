﻿var myapp;
(function () {
    myapp = angular.module('myangularapp', ['ui.grid', 'ui.grid.exporter', 'ui.grid.selection', 'ui.grid.moveColumns', 'ui.grid.exporter', 'ui.grid.importer', 'ui.grid.grouping', 'ui.grid.pagination',]);
})();