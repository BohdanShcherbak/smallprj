﻿myapp.service('crudservice', function ($http) {

    this.getAllUsers = function () {
        return $http.get("/Home/GetAllUsers");
    }
    this.saveUser = function (Users) {
        var save = $http({
            method: 'POST',
            url: '/Home/CreateUser/',
            data: Users
        });
        return save;
    }
    this.deleteUser = function (UserId) {
        var deleterecord = $http({
            method: 'POST',
            url: "/Home/DeleteUser?UserId=" + UserId
        });
        return deleterecord;
    }


});