﻿myapp.controller('crudcontroller', function ($scope, crudservice, uiGridConstants) {

    var Users = crudservice.getAllUsers();

    $scope.Users;
    $scope.Users = {};
    $scope.gridOptions = {
        enableCellEditOnFocus: true,
        enableColumnResizing: true,
        enableGridMenu: true,
        showColumnFooter: true,
        enablePaginationControls: true,
        enableRowSelection: true,
        onRegisterApi: function (gridApi) { $scope.gridApi = gridApi; },
        paginationPageSizes: [10, 25, 50, 100],
        paginationPageSize: 10,

        importerDataAddCallback: function importerDataAddCallback(grid, newObjects) {
            $scope.myData = $scope.data.concat(newObjects);
        },
        columnDefs: [

            { field: 'UserName' },
            { field: 'Login' },
            { field: 'Password' }
           

        ],

    };
    

   
    loadUserss();
    function loadUserss() {
        var Users = crudservice.getAllUsers();
        Users.then(function (data) {
            $scope.Users = data.data;
            $scope.gridOptions.data = $scope.Users;
           

        },
            function () {
                console("Oops..", "Error occured while loading", "error");
            });
    }

    //open create dialog
    $scope.showAlertDetails = function () {
        ngDialog.openConfirm({
            template: 'adduser',
            controller: 'crudcontroller',
            className: 'ngdialog-theme-default',
            scope: $scope,
            closeByNavigation: true,
        }).then(function (User) {
            var saveuser = crudservice.saveUser(User);
            saveuser.then(function (d) {

                $scope.User;
                loadUserss();

            });
        });
    }


    $scope.openDeleteDialog = function () {
        ngDialog.openConfirm({
            template: 'deluser',
            controller: 'crudcontroller',
            className: 'ngdialog-theme-default',
            scope: $scope,

            closeByNavigation: true,
        }).then(function (User) {
            var deleterecord = crudservice.deleteUser($scope.UserId);
            deleterecord.then(function (d) {
                $scope.User;
                loadUserss();
            },
                function () {
                    console("Oops..", "Error occured while loading", "error");
                });
        });
    }

    //close dialog window
    $scope.closeAll = function () {
        ngDialog.closeAll();
    };



   
});