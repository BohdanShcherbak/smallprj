﻿using DAL.Model;
using FastMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace BAL.DTO
{
    public class Mapper
    {
        DB context = new DB();
        public ViewModel MappUser(User user)
        {

            ViewModel usertDto = TypeAdapter.Adapt<User, ViewModel>(user);

            return usertDto;
        }
        public IQueryable<ViewModel> ListUser()
        {
            var users = context.Users.Project().To<ViewModel>().ToList();

            var userList = context.Users.Select(s => new ViewModel()
            {
                UserId = s.UserId,
                UserName = s.UserName,
                Login = s.Login,
                Password = s.Password

            });
            return userList;
        }
    }
}
